Calculs matriciels<br/>
<br/>
Objectifs : créer sa première librairie python perso commenter son code et générer une doc automatiquement à partir des commentaires réaliser des tests unitaires (TU)<br/>
<br/>
Instructions :<br/>
<br/>
Faire la fonction matrice() qui prend en paramètre le nombre de lignes et de colonnes de la matrice à créer. Elle retourne une liste à deux dimensions qui représente la matrice, toutes les valeurs valent 0 par défaut.<br/>
<br/>
Faire la fonction change_valeur() qui prend en paramètre la ligne et la colonne dans la matrice de l'élément à changer et sa nouvelle valeur, et la matrice. Elle retourne la matrice modifiée.<br/>
<br/>
Faire une fonction matrice_identite() qui prend en paramètre la taille de la matrice identité. Elle crée la matrice et la retourne.<br/>
<br/>
Faire une fonction print_matrice() qui prend en paramètre la matrice et l'affiche dans la console.<br/>
<br/>
Faire une fonction dimension_check() qui prend en paramètre 2 matrices et qui retourne True si elles sont de même dimension, False sinon.<br/>
<br/>
Faire une fonction somme_matrice() qui prend en paramètre 2 matrices et en retourne la somme. Cette opération n'est possible que si les 2 matrices sont de même dimension, si ce n'est pas le cas lever une exception de type MatriceDimensionError().<br/>
<br/>
Faire une fonction is_element() qui prend en paramètre une matrice et un nombre, et vérifie si ce nombre est dans la matrice. Si c'est le cas on retourne sa position x, y.<br/>
<br/>
Faire une fonction produit_scalaire(), qui prend en paramètre une matrice et un nombre entier ou décimal, et en retourne le produit scalaire.<br/>
<br/>
Faire une fonction produit_matriciel() qui prend en paramètre 2 matrices, et en retourne le produit matriciel. Vérifier si les dimensions des 2 matrices sont cohérentes pour le produit matriciel, si ce n'est pas le cas lever une exception de type MatriceDimensionError().<br/>
<br/>
Pour chaque fonction, rédiger une docstring qui servira à la documentation. Générer la doc avec Sphinx pour python. Pour chaque fonction, rédiger un TU<br/>
<br/>
