# Fonction qui premet de crée une matrice 3X3
def create_matrice(nb_lines, nb_cols):
    matrice = []

    for i in range(nb_lines):
        line = [0] * nb_cols
        matrice.append(line)
    return matrice

# Utilisation de la fonction pour crée la matrice
nb_lines = 3
nb_cols = 3
my_matrice = create_matrice(nb_lines, nb_cols)

# Fonction qui permet de changer les valeurs dans la matrice
def change_value(new_matrice, nb_lines, nb_cols, new_value):
    if 0 <= nb_lines < len(new_matrice) and 0 <= nb_cols < len(new_matrice[0]):
        new_matrice[nb_lines][nb_cols] = new_value
    else:
        print("Erreur : L'indice de ligne ou de colonne est hors de la plage de la matrice.")
    return new_matrice

# On crée une matrice avec la fonction create_matrice()
new_matrice = create_matrice(3,3)
# Modifiez la valeur à la ligne 1, colonne 2 en 5
new_matrice = change_value(new_matrice, 0, 1, 5)


# Fonction pour crée une matrice Identite
def matrice_identite(size):
    matrice_identite = create_matrice(size, size)  # Crée une matrice de la taille spécifiée remplie de zéros

    # On remplit la diagonale avec des 1
    for i in range(size):
        matrice_identite[i][i] = 1
    return matrice_identite

size = 4
matrice_id = matrice_identite(size)

def print_matrice(p_matrice):
    for line in p_matrice:
        for value in line:
            print(value, end="") # Utilisez end=" " pour imprimer les éléments de la ligne sans saut de ligne
        print() # Imprimez une nouvelle ligne après chaque ligne de la matrice

p_matrice = create_matrice(3, 3) # Créez une matrice 3x3 initialisée à 0
# Modifiez quelques valeurs dans la matrice
p_matrice[0][0] = 1 # On rajoute un 1 la la ligne [0], cellue [0]
p_matrice[1][1] = 2 # On rajoute un 2 la la ligne [2], cellue [2]
p_matrice[2][2] = 3 # On rajoute un 1 la la ligne [3], cellue [3]


def matrice_check(matrice_1, matrice_2):
    m1_rows, m1_cols = len(matrice_1), len(matrice_1[0]) if matrice_1 else 0
    m2_rows, m2_cols = len(matrice_2), len(matrice_2[0]) if matrice_2 else 0

    return m1_rows == m2_rows and m1_cols == m2_cols

# Utilisation de la fonction
matrice_1 = create_matrice(3, 3)
matrice_2 = create_matrice(4, 4)
# Vérifiez si les deux matrices ont la même dimension
resultat = matrice_check(matrice_1, matrice_2)


# Faire une fonction somme_matrice() qui prend en paramètre 2 matrices et en retourne la somme.
# Cette opération n'est possible que si les 2 matrices sont de même dimension.
# Si ce n'est pas le cas lever une exception de type MatriceDimensionError().
class MatriceDimensionError(Exception):
    def __init__(self, message="Les matrices n'ont pas la même dimension."):
        super().__init__(message)

# Faire la somme de deux matrice
def matrice_somme(s_matrice_1, s_matrice_2):
    if not matrice_check(s_matrice_1, s_matrice_2):
        raise MatriceDimensionError()

    nb_rows, nb_cols = len(s_matrice_1), len(s_matrice_2[0])
    resultat = create_matrice(nb_rows, nb_cols)

    for i in range(nb_rows):
        for j in range(nb_cols):
            resultat[i][j] = s_matrice_1[i][j] + s_matrice_2[i][j]

    return resultat

# Exemple d'utilisation de la fonction :
s_matrice_1 = [[1, 2, 3], [4, 5, 6]]
s_matrice_2 = [[7, 8, 9], [9, 8, 2]]


def is_element(comp_matrice, number_find):
    for x in range(len(comp_matrice)):
        for y in range(len(comp_matrice[0])):
            if comp_matrice[x][y] == number_find:
                return  x, y
    return None # Retourne None si le nombre n'est pas trouvé dans la matrice

# Exemple
comp_matrice = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]
# Nombre recherché
number_find = 5
position = is_element(comp_matrice, number_find)



def produit_scalaire(scal_matrice, produit):
    resultat = [] # On crée une matrice pour y stocker le produit scalaire
    for line in scal_matrice:
        new_line = [element * produit for element in line]
        resultat.append(new_line)
    return resultat
# Exemple d'utilisation
scal_matrice = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]
produit = 2
scal_result = produit_scalaire(scal_matrice, produit)


if __name__ == '__main__':

    # Affiche la matrice 3x3 crée avec la fonction "create_matrice()"
    print("Matrice 3*3")
    for ligne in my_matrice:
        print(ligne)

    print("\n")

    # Affiche la matrice 3x3 modifier avec la fonction "change_value()"
    print("Matrice 3*3 modifier, on as changer la valeur 5 situer a l'index de")
    for ligne in new_matrice:
        print(ligne)

    print("\n")

    # Affichez la matrice identité
    print("Matrice 4x4 crée avec la fonction create_matrice()")
    for ligne in matrice_id:
        print(ligne)

    print("\n")
    print("Affiche la matrice 3x3 afvec la fonction print\n"
          "La matrice a ete crée avec la fonction create_matrice()")
    print_matrice(p_matrice)

    print("\n")
    print("Fonction qui compare deux matrice pour vérifié si elle sont de meme taille")
    print("Dans cette exmeple j'ai deux matrice une de 3x3 et une de 4x4")
    print("Le resutat est donc False, en effet les deux matrice ne sont pas de meme taille")
    print(resultat)

    print("\n")
    print("Faire la somme de deux matrice")
    print("La matrice 1 : [[1, 2, 3], [4, 5, 6]]")
    print("Opération :      +  +  +    +  +  +")
    print("La matrice 2 : [[7, 8, 9], [9, 7, 2]]")
    try:
        resultat = matrice_somme(s_matrice_1, s_matrice_2)
        for ligne in resultat:
            print(ligne)
    except MatriceDimensionError as e:
        print(e)  # Cela devrait afficher "Les matrices n'ont pas la même dimension."

    print("\n")
    print("Chercher un nombre ou chiffre dans une matrice")
    print("Ici le nombre chercher est 5")
    if position:
        print(f"Le nombre {number_find} se trouve à la position {position}")
    else:
        print(f"Le nombre {number_find} n'a pas été trouvé dans la matrice.")

    print("\n")
    print("Fonction de produit scalaire, le nombre scalaire est 2 dans cette exemple")
    # Affichez le produit scalaire
    for ligne in scal_result:
        print(ligne)